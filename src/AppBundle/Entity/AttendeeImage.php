<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ImageBundle\Entity\Uploads;

/**
 * AttendeeImage
 *
 * @ORM\Table(name="attendee_image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttendeeImageRepository")
 */
class AttendeeImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Uploads
     *
     * @ORM\ManyToOne(
     *     targetEntity="ImageBundle\Entity\Uploads",
     *     inversedBy="image"
     * )
     * @ORM\JoinColumn(
     *     name="upload_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $upload;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Event",
     *     inversedBy="attendeeImage"
     * )
     * @ORM\JoinColumn(
     *     name="event_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $event;

    /**
     * @var Attendee
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Attendee",
     *     inversedBy="attendeeImage"
     * )
     * @ORM\JoinColumn(
     *     name="attendee_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $attendee;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uploadId
     *
     * @param integer $uploadId
     * @return AttendeeImage
     */
    public function setUploadId($uploadId)
    {
        $this->uploadId = $uploadId;

        return $this;
    }

    /**
     * Get uploadId
     *
     * @return integer
     */
    public function getUploadId()
    {
        return $this->uploadId;
    }

    /**
     * Set attendeeId
     *
     * @param integer $attendeeId
     * @return AttendeeImage
     */
    public function setAttendeeId($attendeeId)
    {
        $this->attendeeId = $attendeeId;

        return $this;
    }

    /**
     * Get attendeeId
     *
     * @return integer
     */
    public function getAttendeeId()
    {
        return $this->attendeeId;
    }
}
