<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Service\Helper\Time;
use AppBundle\Entity\AttendeeStatus;

/**
 * EventAttendee
 *
 * @ORM\Table(name="event_attendee",
 *     indexes={
 *         @ORM\Index(name="IDX_EVENT_ATTENDEE_CREATED_AT", columns={"created_at"}),
 *         @ORM\Index(name="IDX_EVENT_ATTENDEE_ATTENDEE_ID", columns={"attendee_id"}),
 *         @ORM\Index(name="IDX_EVENT_ATTENDEE_EVENT_ID", columns={"event_id"}),
 *         @ORM\Index(name="IDX_EVENT_ATTENDEE_IN_OUT", columns={"in_out"}),
 *         @ORM\Index(name="IDX_EVENT_ATTENDEE_TYPE", columns={"type"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventAttendeeRepository")
 */
class EventAttendee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Event",
     *     inversedBy="eventAttendee"
     * )
     * @ORM\JoinColumn(
     *     name="event_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $event;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Attendee",
     *     inversedBy="eventAttendee"
     * )
     * @ORM\JoinColumn(
     *     name="attendee_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $attendee;

    /**
     * @var AttendeeStatus
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\AttendeeStatus",
     *     inversedBy="eventAttendee"
     * )
     * @ORM\JoinColumn(
     *     name="in_out",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $inOut;

    /**
     * @var AttendeeType
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\AttendeeType",
     *     inversedBy="eventAttendee"
     * )
     * @ORM\JoinColumn(
     *     name="type",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * Event Attendee Constructor.
     * @param Event          $event
     * @param Attendee       $attendee
     * @param AttendeeType   $type
     * @param AttendeeStatus $inOut
     */
    public function __construct(
        Event $event,
        Attendee $attendee,
        AttendeeType $type,
        AttendeeStatus $inOut
    ) {
        $this->setEvent($event);
        $this->setAttendee($attendee);
        $this->setAttendeeType($type);
        $this->setAttendeeStatus($inOut);
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(Time::getUtcTime());
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set event
     *
     * @param Event $event
     *
     * @return EventAttendee
     */
    public function setEvent(Event $event)
    {
        $this->event = $event;
        $event->addEventAttendee($this);

        return $this;
    }

    /**
     * Get an event
     *
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set attendee
     *
     * @param Attendee $attendee
     *
     * @return EventAttendee
     */
    public function setAttendee(Attendee $attendee)
    {
        $this->attendee = $attendee;
        $attendee->addEventAttendee($this);

        return $this;
    }

    /**
     * Get an attendee
     *
     * @return Attendee
     */
    public function getAttendee()
    {
        return $this->attendee;
    }

    /**
     * Set status
     *
     * @param AttendeeStatus $inOut
     *
     * @return EventAttendee
     */
    public function setAttendeeStatus(AttendeeStatus $inOut)
    {
        $this->inOut = $inOut;
        $inOut->addEventAttendee($this);

        return $this;
    }

    /**
     * Get an attendee status
     * 
     * @return AttendeeStatus
     */
    public function getAttendeeStatus()
    {
        return $this->inOut;
    }

    /**
     * Set type
     *
     * @param AttendeeType $type
     *
     * @return EventAttendee
     */
    public function setAttendeeType(AttendeeType $type)
    {
        $this->type = $type;
        $type->addEventAttendee($this);

        return $this;
    }

    /**
     * Get attendee type
     *
     * @return AttendeeType
     */
    public function getAttendeeType()
    {
        return $this->attendeeType;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return EventAttendee
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
