<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Selectable;

/**
 * Venue
 *
 * @ORM\Table(name="venues",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="UNIQ_VENUE_NAME", columns={"name"}),
 *         @ORM\UniqueConstraint(name="UNIQ_VENUE_ADDRESS", columns={"address"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VenueRepository")
 */
class Venue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var VenueAddress
     *
     * @ORM\Column(name="address", type="integer")
     *
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Entity\VenueAddress",
     *     inversedBy="venue"
     * )
     * @ORM\JoinColumn(name="address", referencedColumnName="id")
     */
    private $address;

    /**
     * @var Event[]|ArrayCollection|Selectable
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Event", mappedBy="venue")
     */
    private $event;

    /**
     * Venue Constructor.
     *
     * @param string $name
     * @param VenueAddress $address
     */
    public function __construct($name, VenueAddress $address)
    {
        $this->setName($name);
        $this->setAddress($address);
        $this->event = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Venue
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Venue Address
     *
     * @param VenueAddress $address
     *
     * @return Venue
     */
    public function setAddress(VenueAddress $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get Venue Address
     *
     * @return VenueAddress
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get event
     * @return Event[]|ArrayCollection|Selectable
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Add an event
     *
     * @param Event $event
     *
     * @return Venue
     */
    public function addEvent(Event $event)
    {
        $this->event->add($event);

        return $this;
    }

    /**
     * Remove an event
     *
     * @param  Event $event
     *
     * @return Venue
     */
    public function removeEvent(Event $event)
    {
        $this->event->removeElement($event);

        return $this;
    }
}
