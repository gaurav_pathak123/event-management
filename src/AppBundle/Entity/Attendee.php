<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Selectable;

/**
 * Attendee
 *
 * @ORM\Table(name="attendees",
 *     indexes={
 *         @ORM\Index(name="IDX_ATTENDEE_FIRSTNAME", columns={"first_name"}),
 *         @ORM\Index(name="IDX_ATTENDEE_LASTNAME", columns={"last_name"}),
 *         @ORM\Index(name="IDX_ATTENDEE_CREATED_AT", columns={"created_at"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="UNIQ_ATTENDEE_EMAIL", columns={"email"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttendeeRepository")
 */
class Attendee extends AbstractUser
{
    const USER_TYPE = 'Attendee';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    protected $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var EventAttendee[]|ArrayCollection|Selectable
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\EventAttendee", mappedBy="attendee")
     */
    private $eventAttendee;

    /**
     * @var AttendeeImage[]|ArrayCollection|Selectable
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\AttendeeImage",
     *     mappedBy="attendee"
     * )
     */
    private $attendeeImage;

    /**
     * Attendee Constuctor.
     *
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     */
    public function __construct($firstName, $lastName, $email)
    {
        parent::__construct($firstName, $lastName, $email);
        $this->eventAttendee = new ArrayCollection;
        $this->attendeeImage = new ArrayCollection;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return self::USER_TYPE;
    }

    /**
     * Get event attendees
     *
     * @return EventAttendee[]|ArrayCollection|Selectable
     */
    public function getEventAttendee()
    {
        return $this->eventAttendee;
    }

    /**
     * Add an event attendee
     *
     * @param EventAttendee $eventAttendee
     */
    public function addEventAttendee(EventAttendee $eventAttendee)
    {
        $this->eventAttendee->add($eventAttendee);

        return $this;
    }

    /**
     * Remove an event attendee
     *
     * @param  EventAttendee $eventAttendee
     */
    public function removeEventAttendee(EventAttendee $eventAttendee)
    {
        $this->eventAttendee->removeElement($eventAttendee);

        return $this;
    }
}
