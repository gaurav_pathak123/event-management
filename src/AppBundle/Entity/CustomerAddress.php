<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\CustomerBranch;

/**
 * CustomerAddress
 *
 * @ORM\Table(name="customer_address",
 *     indexes={
 *         @ORM\Index(name="IDX_CUSTOMER_ADDRESS_ADDRESS1", columns={"address1"}),
 *         @ORM\Index(name="IDX_CUSTOMER_ADDRESS_ADDRESS2", columns={"address2"}),
 *         @ORM\Index(name="IDX_CUSTOMER_ADDRESS_ZIP_CODE", columns={"zip_code"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerAddressRepository")
 */
class CustomerAddress extends AbstractAddress
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="string", length=255)
     */
    protected $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    protected $address2;

    /**
     * @var int
     *
     * @ORM\Column(name="country", type="integer", nullable=true)
     */
    protected $country;

    /**
     * @var int
     *
     * @ORM\Column(name="province", type="integer", nullable=true)
     */
    protected $province;

    /**
     * @var int
     *
     * @ORM\Column(name="city", type="integer", nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=255, nullable=true)
     */
    protected $zipCode;

    /**
     * @var CustomerBranch
     *
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Entity\CustomerBranch",
     *     inversedBy="customerAddress"
     * )
     * @ORM\JoinColumn(name="customer_branch_id", referencedColumnName="address")
     */
    private $customerBranch;

    /**
     * Customer Address Constructor.
     *
     * @param string $address1
     * @param string $address2
     * @param integer $country
     * @param integer $province
     * @param integer $city
     * @param integer $zipCode
     * @param CustomerBranch $customerBranch
     */
    public function __construct($address1,
        $address2 = null,
        $country = null,
        $province = null,
        $city = null,
        $zipCode = null,
        CustomerBranch $customerBranch
    ) {
        parent::__construct($address1, $address2, $country, $province, $city, $zipCode);
        $this->setCustomerBranch($customerBranch);
    }

    /**
     * Set Customer Branch
     *
     * @param CustomerBranch $customerBranch
     *
     * @return CustomerAddress
     */
    public function setCustomerBranch(CustomerBranch $customerBranch)
    {
        $this->customerBranch = $customerBranch;

        return $this;
    }

    /**
     * Get Customer Branch
     *
     * @return CustomerBranch
     */
    public function getCustomerBranch()
    {
        return $this->customerBranch;
    }
}
