<?php
namespace AppBundle\Entity;

/**
 * Abstract Address Class
 */
abstract class AbstractAddress
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $address1;

    /**
     * @var string|null
     */
    protected $address2;

    /**
     * @var int|null
     */
    protected $country;

    /**
     * @var int|null
     */
    protected $province;

    /**
     * @var int|null
     */
    protected $city;

    /**
     * @var string|null
     */
    protected $zipCode;

    /**
     * Abstract Address Constructor.
     * @param string $address1
     * @param string $address2
     * @param integer $country
     * @param integer $province
     * @param integer $city
     * @param integer $zipCode
     */
    public function __construct($address1,
        $address2 = null,
        $country = null,
        $province = null,
        $city = null,
        $zipCode = null
    ) {
        $this->setAddress1($address1);
        $this->setAddress2($address2);
        $this->setCountry($country);
        $this->setProvince($province);
        $this->setCity($city);
        $this->setZipCode($zipCode);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return CustomerAddress
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string|null $address2
     * @return CustomerAddress
     */
    public function setAddress2($address2 = null)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string|null
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set country
     *
     * @param integer|null $country
     * @return CustomerAddress
     */
    public function setCountry($country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return integer|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set province
     *
     * @param integer|null $province
     * @return CustomerAddress
     */
    public function setProvince($province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return integer|null
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set city
     *
     * @param integer|null $city
     * @return CustomerAddress
     */
    public function setCity($city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return integer|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set zipCode
     *
     * @param string|null $zipCode
     * @return CustomerAddress
     */
    public function setZipCode($zipCode = null)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }
}
