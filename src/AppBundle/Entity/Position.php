<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Selectable;

/**
 * Position
 *
 * @ORM\Table(name="positions",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="UNIQ_POSITION_NAME", columns={"name"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PositionRepository")
 */
class Position
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var CustomerEmployee[]|ArrayCollection\Selectable
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\CustomerEmployee",
     *     mappedBy="position"
     * )
     */
    private $customerEmployee;

    /**
     * Position Constructor.
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->setName($name);
        $this->customerEmployee = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Position
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add a customer employee
     *
     * @param CustomerEmployee $customerEmployee
     *
     * @return Position
     */
    public function addCustomerEmployee(CustomerEmployee $customerEmployee)
    {
        $this->customerEmployee->add($customerEmployee);

        return $this;
    }

    /**
     * Remove customer employee
     *
     * @param  CustomerEmployee $customerEmployee
     *
     * @return Position
     */
    public function removeCustomerEmployee(CustomerEmployee $customerEmployee)
    {
        $this->customerEmployee->removeElement($customerEmployee);

        return $this;
    }
}
