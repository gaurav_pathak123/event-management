<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Selectable;
use AppBundle\Service\Helper\Time;

/**
 * CustomerBranch
 *
 * @ORM\Table(name="customer_branch",
 *     indexes={
 *         @ORM\Index(name="IDX_CUSTOMER_BRANCH_NAME", columns={"name"}),
 *         @ORM\Index(name="IDX_CUSTOMER_BRANCH_CREATED_AT", columns={"created_at"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="UNIQ_CUSTOMER_BRANCH_CUSTOMER_ADDRESS", columns={"address"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerBranchRepository")
 */
class CustomerBranch
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var CustomerAddress
     *
     * @ORM\Column(name="address", type="integer")
     *
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Entity\CustomerAddress",
     *     inversedBy="customerBranch"
     * )
     * @ORM\JoinColumn(name="address", referencedColumnName="id")
     */
    private $address;

    /**
     * @var CustomerEmployee[]|ArrayCollection\Selectable
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\CustomerEmployee",
     *     mappedBy="customerBranch"
     * )
     */
    private $customerEmployee;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Customer",
     *     inversedBy="customerBranch"
     * )
     * @ORM\JoinColumn(
     *     name="customer_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $customer;

    /**
     * Customer Employee Constructor.
     *
     * @param string $name
     * @param Customer $customer
     * @param CustomerAddress $address
     */
    public function __construct($name, Customer $customer, CustomerAddress $address)
    {
        $this->setName($name);
        $this->setCustomer($customer);
        $this->setAddress($address);
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(Time::getUtcTime());
        }
        $this->customerEmployee = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CustomerBranch
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Customer
     *
     * @param Customer $customer
     *
     * @return CustomerBranch
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        $customer->addCustomerBranch($this);

        return $this;
    }

    /**
     * Get Customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CustomerBranch
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set address
     *
     * @param CustomerAddress $address
     *
     * @return CustomerBranch
     */
    public function setAddress(CustomerAddress $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return CustomerAddress
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get customer employee
     *
     * @return CustomerEmployee|ArrayCollection|Selectable
     */
    public function getCustomerEmployee()
    {
        return $this->customerEmployee;
    }

    /**
     * Add an customer employee
     *
     * @param CustomerEmployee $customerEmployee
     */
    public function addCustomerEmployee(CustomerEmployee $customerEmployee)
    {
        $this->customerEmployee->add($customerEmployee);
    }

    /**
     * Remove an customer employee
     *
     * @param  CustomerEmployee $customerEmployee
     */
    public function removeCustomerEmployee(CustomerEmployee $customerEmployee)
    {
        $this->customerEmployee->removeElement($customerEmployee);
    }
}
