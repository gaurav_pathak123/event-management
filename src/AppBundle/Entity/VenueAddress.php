<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VenueAddress
 *
 * @ORM\Table(name="venue_address",
 *     indexes={
 *         @ORM\Index(name="IDX_VENUE_ADDRESS_ADDRESS1", columns={"address1"}),
 *         @ORM\Index(name="IDX_VENUE_ADDRESS_ADDRESS2", columns={"address2"}),
 *         @ORM\Index(name="IDX_VENUE_ADDRESS_ZIP_CODE", columns={"zip_code"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VenueAddressRepository")
 */
class VenueAddress extends AbstractAddress
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="string", length=255)
     */
    protected $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    protected $address2;

    /**
     * @var int
     *
     * @ORM\Column(name="country", type="integer", nullable=true)
     */
    protected $country;

    /**
     * @var int
     *
     * @ORM\Column(name="province", type="integer", nullable=true)
     */
    protected $province;

    /**
     * @var int
     *
     * @ORM\Column(name="city", type="integer", nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=255, nullable=true)
     */
    protected $zipCode;

    /**
     * @var Venue
     *
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Entity\Venue",
     *     inversedBy="address"
     * )
     * @ORM\JoinColumn(name="venue_id", referencedColumnName="address")
     */
    private $venue;
}
