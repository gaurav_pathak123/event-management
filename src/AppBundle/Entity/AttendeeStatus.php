<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Selectable;
use AppBundle\Entity\EventAttendee;

/**
 * AttendeeStatus
 *
 * @ORM\Table(name="attendee_status",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="UNIQ_ATTENDEE_STATUS_NAME", columns={"name"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttendeeStatusRepository")
 */
class AttendeeStatus
{
    const ATTENDEE_STATUS_IN = 'in';
    const ATTENDEE_STATUS_OUT = 'out';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var EventAttendee[]|ArrayCollection|Selectable
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\EventAttendee", mappedBy="inOut")
     */
    private $eventAttendee;

    /**
     * Attendee Status Constructor.
     *
     * @param string $name
     *
     */
    public function __construct($name = self::ATTENDEE_STATUS_IN)
    {
        $this->setName($name);
        $this->eventAttendee = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AttendeeStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get available status
     *
     * @return array
     */
    public function getStatus()
    {
        return [
            self::ATTENDEE_STATUS_IN,
            self::ATTENDEE_STATUS_OUT,
        ];
    }

    /**
     * Get event attendees
     *
     * @return EventAttendee[]|ArrayCollection|Selectable
     */
    public function getEventAttendee()
    {
        return $this->eventAttendee;
    }

    /**
     * Add an event attendee
     *
     * @param EventAttendee $eventAttendee
     */
    public function addEventAttendee(EventAttendee $eventAttendee)
    {
        $this->eventAttendee->add($eventAttendee);
    }

    /**
     * Remove an event attendee
     *
     * @param  EventAttendee $eventAttendee
     */
    public function removeEventAttendee(Event $eventAttendee)
    {
        $this->eventAttendee->removeElement($eventAttendee);
    }
}
