<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Selectable;
use AppBundle\Service\Helper\Time;

/**
 * Event
 *
 * @ORM\Table(name="events",
 *     indexes={
 *         @ORM\Index(name="IDX_EVENT_CUSTOMER", columns={"customer_id"}),
 *         @ORM\Index(name="IDX_EVENT_TITLE", columns={"title"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var Venue
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Venue",
     *     inversedBy="event"
     * )
     * @ORM\JoinColumn(
     *     name="venue_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $venue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Customer",
     *     inversedBy="event"
     * )
     * @ORM\JoinColumn(
     *     name="customer_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $customer;

    /**
     * @var EventAttendee[]|ArrayCollection|Selectable
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\EventAttendee", mappedBy="event")
     */
    private $eventAttendee;

    /**
     * @var AttendeeImage[]|ArrayCollection|Selectable
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\AttendeeImage",
     *     mappedBy="event"
     * )
     */
    private $attendeeImage;

    /**
     * Event Constructor.
     *
     * @param string $title
     * @param Customer $customer
     * @param Venue $venue
     */
    public function __construct($title, Customer $customer, Venue $venue)
    {
        $this->setTitle($title);
        $this->setCustomer($customer);
        $this->setVenue($venue);
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(Time::getUtcTime());
        }
        $this->setUpdatedAt(Time::getUtcTime());
        $this->eventAttendee = new ArrayCollection;
        $this->attendeeImage = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set venue
     *
     * @param Venue $venue
     * @return Event
     */
    public function setVenue(Venue $venue)
    {
        $this->venue = $venue;
        $venue->addEvent($this);

        return $this;
    }

    /**
     * Get venue
     *
     * @return string
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        $customer->addEvent($this);

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Event
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Event
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get event attendees
     *
     * @return EventAttendee|ArrayCollection|Selectable
     */
    public function getEventAttendee()
    {
        return $this->eventAttendee;
    }

    /**
     * Add an event attendee
     *
     * @param EventAttendee $eventAttendee
     */
    public function addEventAttendee(EventAttendee $eventAttendee)
    {
        $this->eventAttendee->add($eventAttendee);
    }

    /**
     * Remove an event attendee
     *
     * @param  EventAttendee $eventAttendee
     */
    public function removeEventAttendee(EventAttendee $eventAttendee)
    {
        $this->eventAttendee->removeElement($eventAttendee);
    }

    /**
     * Get event images
     *
     * @return AttendeeImage|ArrayCollection|Selectable
     */
    public function getAttendeeImage()
    {
        return $this->attendeeImage;
    }

    /**
     * Add an event image
     *
     * @param AttendeeImage $attendeeImage
     */
    public function addAttendeeImage(AttendeeImage $attendeeImage)
    {
        $this->attendeeImage->add($attendeeImage);
    }

    /**
     * Remove an event image
     *
     * @param  AttendeeImage $attendeeImage
     */
    public function removeAttendeeImage(AttendeeImage $attendeeImage)
    {
        $this->attendeeImage->removeElement($attendeeImage);
    }
}
