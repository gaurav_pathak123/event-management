<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Selectable;

/**
 * AttendeeType
 *
 * @ORM\Table(name="attendee_types",
 *     indexes={
 *         @ORM\Index(name="IDX_ATTENDEE_TYPE_CUSTOMER", columns={"customer_id"}),
 *         @ORM\Index(name="IDX_ATTENDEE_TYPE_NAME", columns={"name"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttendeeTypeRepository")
 */
class AttendeeType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Customer",
     *     inversedBy="attendeeType"
     * )
     * @ORM\JoinColumn(
     *     name="customer_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $customer;

    /**
     * @var EventAttendee[]|ArrayCollection\Selectable
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\EventAttendee", mappedBy="type")
     */
    private $eventAttendee;

    /**
     * Attendee Type Constructor.
     *
     * @param string $name
     * @param Customer $customer
     */
    public function __construct($name, Customer $customer)
    {
        $this->setName($name);
        $this->setCustomer($customer);
        $this->eventAttendee = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AttendeeType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return AttendeeType
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        $customer->addAttendeeType($this);

        return $this;
    }

    /**
     * Get Customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Get event attendees
     *
     * @return EventAttendee[]|ArrayCollection|Selectable
     */
    public function getEventAttendee()
    {
        return $this->eventAttendee;
    }

    /**
     * Add an event attendee
     *
     * @param EventAttendee $eventAttendee
     */
    public function addEventAttendee(EventAttendee $eventAttendee)
    {
        $this->eventAttendee->add($eventAttendee);
    }

    /**
     * Remove an event attendee
     *
     * @param  EventAttendee $eventAttendee
     */
    public function removeEventAttendee(EventAttendee $eventAttendee)
    {
        $this->eventAttendee->removeElement($eventAttendee);
    }
}
