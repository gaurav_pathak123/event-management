<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Selectable;
use AppBundle\Service\Helper\Time;

/**
 * Customer
 *
 * @ORM\Table(name="customers",
 *     indexes={
 *         @ORM\Index(name="IDX_CUSTOMER_CREATED_AT", columns={"created_at"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="UNIQ_CUSTOMER_ORGANIZATION", columns={"organization"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="organization", type="string", length=255)
     */
    private $organization;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var Event[]|ArrayCollection|Selectable
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Event", mappedBy="customer")
     */
    private $event;

    /**
     * @var AttendeeType[]|ArrayCollection|Selectable
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AttendeeType", mappedBy="customer")
     */
    private $attendeeType;

    /**
     * @var CustomerBranch[]|ArrayCollection|Selectable
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CustomerBranch", mappedBy="customer")
     */
    private $customerBranch;

    /**
     * Customer Constructor.
     *
     * @param string $organization
     */
    public function __construct($organization)
    {
        $this->setOrganization($organization);
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(Time::getUtcTime());
        }
        $this->event = new ArrayCollection;
        $this->attendeeType = new ArrayCollection;
        $this->customerBranch = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organization
     *
     * @param string $organization
     * @return Customer
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return string
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Customer
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get attendee types
     *
     * @return AttendeeType|ArrayCollection|Selectable
     */
    public function getAttendeeType()
    {
        return $this->attendeeType;
    }

    /**
     * Add an attendee type
     *
     * @param AttendeeType $attendeeType
     */
    public function addAttendeeType(AttendeeType $attendeeType)
    {
        $this->attendeeType->add($attendeeType);
    }

    /**
     * Remove an attendee type
     *
     * @param  AttendeeType $attendeeType
     */
    public function removeAttendeeType(AttendeeType $attendeeType)
    {
        $this->attendeeType->removeElement($attendeeType);
    }

    /**
     * Get events
     *
     * @return Event|ArrayCollection|Selectable
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Add an event
     *
     * @param Event $event
     */
    public function addEvent(Event $event)
    {
        $this->event->add($event);
    }

    /**
     * Remove an event
     *
     * @param  Event $event
     */
    public function removeEvent(Event $event)
    {
        $this->event->removeElement($event);
    }

    /**
     * Get customer branch
     *
     * @return CustomerBranch|ArrayCollection|Selectable
     */
    public function getCustomerBranch()
    {
        return $this->attendeeType;
    }

    /**
     * Add a customer branch
     *
     * @param CustomerBranch $customerBranch
     */
    public function addCustomerBranch(CustomerBranch $customerBranch)
    {
        $this->customerBranch->add($customerBranch);
    }

    /**
     * Remove a customer branch
     *
     * @param  CustomerBranch $customerBranch
     */
    public function removeCustomerBranch(CustomerBranch $customerBranch)
    {
        $this->customerBranch->removeElement($customerBranch);
    }
}
