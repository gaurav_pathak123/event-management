<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomerEmployee
 *
 * @ORM\Table(name="customer_employees",
 *     indexes={
 *         @ORM\Index(name="IDX_CUSTOMER_EMPLOYEE_FIRSTNAME", columns={"first_name"}),
 *         @ORM\Index(name="IDX_CUSTOMER_EMPLOYEE_LASTNAME", columns={"last_name"}),
 *         @ORM\Index(name="IDX_CUSTOMER_EMPLOYEE_CREATED_AT", columns={"created_at"}),
 *         @ORM\Index(name="IDX_CUSTOMER_EMPLOYEE_CUSTOMER_BRANCH", columns={"customer_branch_id"}),
 *         @ORM\Index(name="IDX_CUSTOMER_EMPLOYEE_POSITION", columns={"position_id"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="UNIQ_CUSTOMER_EMPLOYEE_EMAIL", columns={"email"})
 *     })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerEmployeeRepository")
 */
class CustomerEmployee extends AbstractUser
{
    const USER_TYPE = 'Customer Employee';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    protected $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var CustomerBranch
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\CustomerBranch",
     *     inversedBy="customerEmployee"
     * )
     * @ORM\JoinColumn(
     *     name="customer_branch_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    private $customerBranch;

    /**
     * @var Position
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Position",
     *     inversedBy="customerEmployee"
     * )
     * @ORM\JoinColumn(
     *     name="position_id",
     *     referencedColumnName="id",
     *     nullable=true
     * )
     */
    private $position;

    /**
     * Customer Employee Constuctor.
     *
     * @param string $firstname
     * @param string $lastName
     * @param string $email
     * @param CustomerBranch $customerBranch
     * @param Position $position
     */
    public function __construct($firstName, $lastName, $email, CustomerBranch $customerBranch, Position $position)
    {
        parent::__construct($firstName, $lastName, $email);
        $this->setCustomerBranch($customerBranch);
        $this->setPosition($position);
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return self::USER_TYPE;
    }

    /**
     * Set Customer Branch
     *
     * @param CustomerBranch $customerBranch
     *
     * @return CustomerEmployee
     */
    public function setCustomerBranch(CustomerBranch $customerBranch)
    {
        $this->customerBranch = $customerBranch;
        $customerBranch->addCustomerEmployee($this);

        return $this;
    }

    /**
     * Get Customer Branch
     *
     * @return CustomerBranch
     */
    public function getCustomerBranch()
    {
        return $this->customerBranch;
    }

    /**
     * Set Employee Position
     *
     * @param Position $position
     *
     * @return CustomerEmployee
     */
    public function setPosition(Position $position)
    {
        $this->position = $position;
        $position->addCustomerEmployee($this);

        return $this;
    }

    /**
     * Get Employee Position
     *
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }
}
