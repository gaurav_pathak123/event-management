<?php
namespace ImageBundle\Service;

use \Exception as Exception;

/**
 * Resize images
 */
class Resize
{
    /**
     * @var string
     */
    private $uploadDirectory;

    /**
     * @var string
     */
    private $contentType;

    /**
     * @var resource
     */
    private $source;

    /**
     * @var resource
     */
    private $resizedImage;

    /**
     * @var string
     */
    private $destination;

    /**
     * @var array
     */
    private $extension = [
        'image/jpeg' => '.jpg',
        'image/bmp' => '.bmp',
        'image/png' => '.png',
        'image/gif' => '.gif',
    ];

    /**
     * Resize Constructor
     *
     * @param string $uploadDirectory
     */
    public function __construct($uploadDirectory)
    {
        $this->uploadDirectory = $uploadDirectory;
    }

    /**
     * Resizes image to defined width
     *
     * @param string $filename
     * @param integer $newWidth
     * @param integer $newHeight
     *
     * @return $string
     */

    public function resize($filename, $newWidth, $newHeight = null)
    {
        $this->contentType = mime_content_type($filename);

        // Get new sizes
        list($width, $height) = getimagesize($filename);

        // calculate aspect ratio
        $aspectRatio = ($width/$height);

        // desiredWidth > originalWidth
        if ($newWidth >= $width && $newHeight != null && $newHeight >= $height) {
            $newWidth = $width;
            $newHeight = $height;
        } else if ($newHeight <= $height && $newHeight != null && $newWidth >= $width) {
            $newWidth = $width;
            $newHeight = $height;
        } else if ($newWidth <= $width && $newHeight != null && $newHeight >= $height) {
            $newHeight = ceil((1 / $aspectRatio) * $newWidth);
        }

        // if desiredWidth < originalWidth
        if ($newHeight == null) {
            $newHeight = ceil((1 / $aspectRatio) * $newWidth);
        }
        $this->destination = $filename . '_' . $newWidth . 'px_x_' . $newHeight . 'px';

        // Load
        $this->resizedImage = imagecreatetruecolor($newWidth, $newHeight);
        // create source images
        $this->imagecreatefromsource($filename);

        // Resize
        imagecopyresampled($this->resizedImage, $this->source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        // save image
        $this->saveImage();
        imagedestroy($this->resizedImage);

        return $this->destination;
    }

    /**
     * Get source image resource from filename and content type
     *
     * @param  string $filename
     */
    private function imagecreatefromsource($filename)
    {
        switch ($this->contentType) {
            case 'image/bmp' :
                $this->source = imagecreatefromwbmp($filename);
                break;

            case 'image/jpeg' :
                $this->source = imagecreatefromjpeg($filename);
                break;

            case 'image/png' :
                $this->source = imagecreatefrompng($filename);
                $this->preserveTransparency();
                break;

            case 'image/gif' :
                $this->source = imagecreatefromgif($filename);
                $this->preserveTransparency();
                break;

            default:
                throw new Exception('The content type does not match.');
                break;
        }
    }

    /**
     * Preserve transparency of the image
     *
     * @param resource $resized
     */
    private function preserveTransparency()
    {
        imagecolortransparent($this->resizedImage, imagecolorallocatealpha($this->resizedImage, 0, 0, 0, 127));
        imagealphablending($this->resizedImage, false);
        imagesavealpha($this->resizedImage, true);
    }

    /**
     * Save image
     */
    private function saveImage()
    {
        $this->destination .= $this->extension[$this->contentType];
        switch ($this->contentType) {
            case 'image/bmp' :
                imagewbmp($this->resizedImage, $this->destination);
                break;

            case 'image/jpeg' :
                imagejpeg($this->resizedImage, $this->destination);
                break;

            case 'image/png' :
                imagepng($this->resizedImage, $this->destination);
                break;

            case 'image/gif' :
                imagegif($this->resizedImage, $this->destination);
                break;

            default:
                throw new Exception('The content type does not match.');
                break;
        }
    }

    /**
     * Returns content type
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }
}
