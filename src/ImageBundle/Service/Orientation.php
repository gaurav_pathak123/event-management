<?php
namespace ImageBundle\Service;

/**
 * Image Orientation
 */
class Orientation
{
    /**
     * Return Orientation
     *
     * @param string $filename
     *
     * @return string
     */
    public function getOrientation($filename)
    {
        list($width, $height) = getimagesize($filename);
        $orientation = 'P';
        if ($width >= $height) {
            $orientation = 'L';
        }
        return $orientation;
    }
}
