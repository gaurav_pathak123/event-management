<?php
namespace ImageBundle\Service\Helper;

use \DateTime as DateTime;
use \DateTimeZone as DateTimeZone;

/**
 * Helper Class to handle time based conversions
 */
class Time
{
    const TIME_DATE_FORMAT = 'Y-m-d H:i:s';
    const TIME_TIMEZONE = 'UTC';

    /**
     * Get UTC Time
     *
     * @param \DateTime $dateTime local time
     *
     * @return \DateTime
     */
    public static function getUtcTime(DateTime $dateTime = null)
    {
        if (null == $dateTime) {
            $dateTime = new DateTime('now');
        }
        return new DateTime($dateTime->format(self::TIME_DATE_FORMAT), new DateTimeZone(self::TIME_TIMEZONE));
    }
}
