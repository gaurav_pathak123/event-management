<?php
namespace ImageBundle\Tests\Service;

use PHPUnit\Framework\TestCase;
use ImageBundle\Service\Resize as Resize;

/**
 * Test for resize
 */
class ResizeTest extends TestCase
{
    /** @var \ImageBundle\Service\Resize */
    private $resizeService;

    /**
     * @var string
     */
    private $destination;

    /**
     * @var string
     */
    private $baseDir;

    /**
     * @var string
     * @width 930
     * @height 1000
     */
    protected $pngImage = 'taj-mahal.png';

    /**
     * @var string
     * @width 1280
     * @height 692
     */
    protected $jpegImage = 'image.jpg';

    /**
     * @var string
     * @width 313
     * @height 308
     */
    protected $gifImage = 'gif-image.gif';

    /** @var TestHelper */
    private static $helper;

    /**
     * @inheritdoc
     */
    public static function setUpBeforeClass()
    {
        self::$helper = new TestHelper();
    }

    /**
     * Add images to test the service
     */
    protected function setUp()
    {
        $this->baseDir = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'Images';
        $this->resizeService = self::$helper->createResizeObject();
    }

    /**
     * @dataProvider testDataProvider
     * @outputBuffering enabled
     */
    public function testResize($filename, $width, $height, $expectedWidth, $expectedHeight, $expectedContentType, $expectedFilename)
    {
        $this->destination = $this->resizeService->resize($this->baseDir . DIRECTORY_SEPARATOR . $filename,
            $expectedWidth,
            $expectedHeight
        );
        $this->assertEquals($expectedContentType, $this->resizeService->getContentType(), 'Content type mismatch');
        //$this->assertEquals($expectedWidth, $width < $expectedWidth ? $width : $expectedWidth);
        $this->assertEquals($expectedFilename, basename($this->destination), 'Filename mismatch');
        $this->assertFileExists($this->destination, 'File not found');
    }

    /**
     * Provide data to function
     * @return [type] [description]
     */
    public function testDataProvider()
    {
        return [
            [ $this->pngImage, 930, 1000, 200, 215, 'image/png', 'taj-mahal.png_200px_x_215px.png' ],
            [ $this->jpegImage, 1280, 692, 200, 108, 'image/jpeg', 'image.jpg_200px_x_108px.jpg' ],
            [ $this->gifImage, 313, 308, 200, 197, 'image/gif', 'gif-image.gif_200px_x_197px.gif' ],
            [ $this->pngImage, 930, 1000, 931, 1000, 'image/png', 'taj-mahal.png_930px_x_1000px.png' ],
        ];
    }

    /** @inheritdoc */
    protected function tearDown()
    {
        if ($this->destination) {
            unlink($this->destination);
        }
    }
}
