<?php
namespace ImageBundle\Tests\Service;

use ImageBundle\Service\Resize as Resize;
use ImageBundle\Service\Orientation as Orientation;

/**
 * Class to create factory objects
 */
class TestHelper
{
    /**
     * @return Resize
     */
    public function createResizeObject()
    {
        return new Resize(realpath('./'. DIRECTORY_SEPARATOR . 'uploads'));
    }

    /**
     * @return Orientation
     */
    public function createOrientationObject()
    {
        return new Orientation();
    }
}
