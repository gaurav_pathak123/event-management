<?php
namespace ImageBundle\Tests\Service;

use PHPUnit\Framework\TestCase;
use ImageBundle\Service\Orientation as Orientation;

/**
 * Test for resize
 */
class OrientationTest extends TestCase
{
    /** @var ImageBundle\Service\Orientation */
    private $orientationService;

    /**
     * @var string
     */
    private $baseDir;

    /**
     * @var string
     */
    protected $pngImage = 'taj-mahal.png';

    /**
     * @var string
     */
    protected $jpegImage = 'image.jpg';

    /**
     * @var string
     */
    protected $gifImage = 'gif-image.gif';

    /** @var TestHelper */
    private static $helper;

    /**
     * @inheritdoc
     */
    public static function setUpBeforeClass()
    {
        self::$helper = new TestHelper();
    }

    /**
     * Add images to test the service
     */
    protected function setUp()
    {
        $this->baseDir = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'Images';
        $this->orientationService = self::$helper->createOrientationObject();
    }

    /**
     * @dataProvider testDataProvider
     * @outputBuffering enabled
     */
    public function testOrientation($filename, $expectedOrientation)
    {
        $orientation = $this->orientationService->getOrientation($this->baseDir . DIRECTORY_SEPARATOR . $filename);
        $this->assertEquals($expectedOrientation, $orientation, 'Orientation mismatch');
    }

    /**
     * Provide data to function
     * @return
     */
    public function testDataProvider()
    {
        return [
            [ $this->pngImage, 'P' ],
            [ $this->jpegImage, 'L' ],
            [ $this->gifImage, 'L' ],
        ];
    }
}
