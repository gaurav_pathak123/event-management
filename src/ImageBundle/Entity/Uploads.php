<?php
namespace ImageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ImageBundle\Service\Helper\Time;

/**
 * Uploads
 *
 * @ORM\Table(name="uploads",
 *     indexes={
 *         @ORM\Index(name="IDX_UPLOADS_CREATED_AT", columns={"created_at"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="UNIQ_UPLOADS_FILEPATH", columns={"file_path"})
 *     })
 * @ORM\Entity(repositoryClass="ImageBundle\Repository\UploadsRepository")
 */
class Uploads
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path", type="string", length=255)
     */
    private $filePath;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="orientation", type="string", length=1)
     */
    private $orientation;

    /**
     * Upload Constructor.
     *
     * @param string $filePath
     * @param string $orientation
     */
    public function __construct($filePath, $orientation)
    {
        $this->setFilePath($filePath);
        $this->setOrientation($orientation);
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(Time::getUtcTime());
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     *
     * @return Uploads
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set Orientation
     *
     * @param string $orientation
     */
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;

        return $this;
    }

    /**
     * Get orientation
     *
     * @return string
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * 
     * @return Uploads
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
