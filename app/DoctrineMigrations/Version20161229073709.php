<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161229073709 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE address_types (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_ADDRESS_TYPE_NAME (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attendees (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_ATTENDEE_FIRSTNAME (first_name), INDEX IDX_ATTENDEE_LASTNAME (last_name), INDEX IDX_ATTENDEE_CREATED_AT (created_at), UNIQUE INDEX UNIQ_ATTENDEE_EMAIL (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE companies (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_COMPANY_NAME (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_types (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_CONTACT_TYPE_NAME (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customers (id INT AUTO_INCREMENT NOT NULL, organization VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_CUSTOMER_CREATED_AT (created_at), UNIQUE INDEX UNIQ_CUSTOMER_ORGANIZATION (organization), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer_address (id INT AUTO_INCREMENT NOT NULL, customer_branch_id INT DEFAULT NULL, address1 VARCHAR(255) NOT NULL, address2 VARCHAR(255) DEFAULT NULL, country INT DEFAULT NULL, province INT DEFAULT NULL, city INT DEFAULT NULL, zip_code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1193CB3F772B3453 (customer_branch_id), INDEX IDX_CUSTOMER_ADDRESS_ADDRESS1 (address1), INDEX IDX_CUSTOMER_ADDRESS_ADDRESS2 (address2), INDEX IDX_CUSTOMER_ADDRESS_ZIP_CODE (zip_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer_branch (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, address INT NOT NULL, INDEX IDX_CUSTOMER_BRANCH_NAME (name), INDEX IDX_CUSTOMER_BRANCH_CREATED_AT (created_at), UNIQUE INDEX UNIQ_CUSTOMER_BRANCH_CUSTOMER_ADDRESS (address), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer_employees (id INT AUTO_INCREMENT NOT NULL, customer_branch_id INT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_CUSTOMER_EMPLOYEE_FIRSTNAME (first_name), INDEX IDX_CUSTOMER_EMPLOYEE_LASTNAME (last_name), INDEX IDX_CUSTOMER_EMPLOYEE_CREATED_AT (created_at), INDEX IDX_CUSTOMER_EMPLOYEE_CUSTOMER_BRANCH (customer_branch_id), UNIQUE INDEX UNIQ_CUSTOMER_EMPLOYEE_EMAIL (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE events (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, title VARCHAR(255) NOT NULL, venue VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_EVENT_CUSTOMER (customer_id), INDEX IDX_EVENT_TITLE (title), INDEX IDX_EVENT_VENUE (venue), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE positions (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_POSITION_NAME (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer_address ADD CONSTRAINT FK_1193CB3F772B3453 FOREIGN KEY (customer_branch_id) REFERENCES customer_branch (address)');
        $this->addSql('ALTER TABLE customer_employees ADD CONSTRAINT FK_117F3B1A772B3453 FOREIGN KEY (customer_branch_id) REFERENCES customer_branch (id)');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574A9395C3F3 FOREIGN KEY (customer_id) REFERENCES customers (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574A9395C3F3');
        $this->addSql('ALTER TABLE customer_address DROP FOREIGN KEY FK_1193CB3F772B3453');
        $this->addSql('ALTER TABLE customer_employees DROP FOREIGN KEY FK_117F3B1A772B3453');
        $this->addSql('DROP TABLE address_types');
        $this->addSql('DROP TABLE attendees');
        $this->addSql('DROP TABLE companies');
        $this->addSql('DROP TABLE contact_types');
        $this->addSql('DROP TABLE customers');
        $this->addSql('DROP TABLE customer_address');
        $this->addSql('DROP TABLE customer_branch');
        $this->addSql('DROP TABLE customer_employees');
        $this->addSql('DROP TABLE events');
        $this->addSql('DROP TABLE positions');
    }
}
