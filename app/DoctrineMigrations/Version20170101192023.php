<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170101192023 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE attendee_image (id INT AUTO_INCREMENT NOT NULL, upload_id INT NOT NULL, event_id INT NOT NULL, attendee_id INT NOT NULL, INDEX IDX_CD08E1EDCCCFBA31 (upload_id), INDEX IDX_CD08E1ED71F7E88B (event_id), INDEX IDX_CD08E1EDBCFD782A (attendee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE uploads (id INT AUTO_INCREMENT NOT NULL, file_path VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, orientation VARCHAR(1) NOT NULL, INDEX IDX_UPLOADS_CREATED_AT (created_at), UNIQUE INDEX UNIQ_UPLOADS_FILEPATH (file_path), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE attendee_image ADD CONSTRAINT FK_CD08E1EDCCCFBA31 FOREIGN KEY (upload_id) REFERENCES uploads (id)');
        $this->addSql('ALTER TABLE attendee_image ADD CONSTRAINT FK_CD08E1ED71F7E88B FOREIGN KEY (event_id) REFERENCES events (id)');
        $this->addSql('ALTER TABLE attendee_image ADD CONSTRAINT FK_CD08E1EDBCFD782A FOREIGN KEY (attendee_id) REFERENCES attendees (id)');
        $this->addSql('ALTER TABLE customer_branch ADD customer_id INT NOT NULL');
        $this->addSql('ALTER TABLE customer_branch ADD CONSTRAINT FK_A43C74E9395C3F3 FOREIGN KEY (customer_id) REFERENCES customers (id)');
        $this->addSql('CREATE INDEX IDX_A43C74E9395C3F3 ON customer_branch (customer_id)');
        $this->addSql('ALTER TABLE customer_employees ADD position_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE customer_employees ADD CONSTRAINT FK_117F3B1ADD842E46 FOREIGN KEY (position_id) REFERENCES positions (id)');
        $this->addSql('CREATE INDEX IDX_CUSTOMER_EMPLOYEE_POSITION ON customer_employees (position_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attendee_image DROP FOREIGN KEY FK_CD08E1EDCCCFBA31');
        $this->addSql('DROP TABLE attendee_image');
        $this->addSql('DROP TABLE uploads');
        $this->addSql('ALTER TABLE customer_branch DROP FOREIGN KEY FK_A43C74E9395C3F3');
        $this->addSql('DROP INDEX IDX_A43C74E9395C3F3 ON customer_branch');
        $this->addSql('ALTER TABLE customer_branch DROP customer_id');
        $this->addSql('ALTER TABLE customer_employees DROP FOREIGN KEY FK_117F3B1ADD842E46');
        $this->addSql('DROP INDEX IDX_CUSTOMER_EMPLOYEE_POSITION ON customer_employees');
        $this->addSql('ALTER TABLE customer_employees DROP position_id');
    }
}
