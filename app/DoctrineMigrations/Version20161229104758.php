<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161229104758 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE attendee_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_ATTENDEE_STATUS_NAME (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE attendee_types (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_ATTENDEE_TYPE_CUSTOMER (customer_id), INDEX IDX_ATTENDEE_TYPE_NAME (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_attendee (id INT AUTO_INCREMENT NOT NULL, event_id INT NOT NULL, attendee_id INT NOT NULL, in_out INT NOT NULL, type INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_EVENT_ATTENDEE_CREATED_AT (created_at), INDEX IDX_EVENT_ATTENDEE_ATTENDEE_ID (attendee_id), INDEX IDX_EVENT_ATTENDEE_EVENT_ID (event_id), INDEX IDX_EVENT_ATTENDEE_IN_OUT (in_out), INDEX IDX_EVENT_ATTENDEE_TYPE (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE attendee_types ADD CONSTRAINT FK_51056C829395C3F3 FOREIGN KEY (customer_id) REFERENCES customers (id)');
        $this->addSql('ALTER TABLE event_attendee ADD CONSTRAINT FK_57BC3CB771F7E88B FOREIGN KEY (event_id) REFERENCES events (id)');
        $this->addSql('ALTER TABLE event_attendee ADD CONSTRAINT FK_57BC3CB7BCFD782A FOREIGN KEY (attendee_id) REFERENCES attendees (id)');
        $this->addSql('ALTER TABLE event_attendee ADD CONSTRAINT FK_57BC3CB76DFE9D62 FOREIGN KEY (in_out) REFERENCES attendee_status (id)');
        $this->addSql('ALTER TABLE event_attendee ADD CONSTRAINT FK_57BC3CB78CDE5729 FOREIGN KEY (type) REFERENCES attendee_types (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event_attendee DROP FOREIGN KEY FK_57BC3CB76DFE9D62');
        $this->addSql('ALTER TABLE event_attendee DROP FOREIGN KEY FK_57BC3CB78CDE5729');
        $this->addSql('DROP TABLE attendee_status');
        $this->addSql('DROP TABLE attendee_types');
        $this->addSql('DROP TABLE event_attendee');
    }
}
