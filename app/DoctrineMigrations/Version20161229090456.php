<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161229090456 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE venues (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, address INT NOT NULL, UNIQUE INDEX UNIQ_VENUE_NAME (name), UNIQUE INDEX UNIQ_VENUE_ADDRESS (address), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE venue_address (id INT AUTO_INCREMENT NOT NULL, venue_id INT DEFAULT NULL, address1 VARCHAR(255) NOT NULL, address2 VARCHAR(255) DEFAULT NULL, country INT DEFAULT NULL, province INT DEFAULT NULL, city INT DEFAULT NULL, zip_code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_BD86985F40A73EBA (venue_id), INDEX IDX_VENUE_ADDRESS_ADDRESS1 (address1), INDEX IDX_VENUE_ADDRESS_ADDRESS2 (address2), INDEX IDX_VENUE_ADDRESS_ZIP_CODE (zip_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE venue_address ADD CONSTRAINT FK_BD86985F40A73EBA FOREIGN KEY (venue_id) REFERENCES venues (address)');
        $this->addSql('DROP INDEX IDX_EVENT_VENUE ON events');
        $this->addSql('ALTER TABLE events ADD venue_id INT NOT NULL, DROP venue');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574A40A73EBA FOREIGN KEY (venue_id) REFERENCES venues (id)');
        $this->addSql('CREATE INDEX IDX_5387574A40A73EBA ON events (venue_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574A40A73EBA');
        $this->addSql('ALTER TABLE venue_address DROP FOREIGN KEY FK_BD86985F40A73EBA');
        $this->addSql('DROP TABLE venues');
        $this->addSql('DROP TABLE venue_address');
        $this->addSql('DROP INDEX IDX_5387574A40A73EBA ON events');
        $this->addSql('ALTER TABLE events ADD venue VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP venue_id');
        $this->addSql('CREATE INDEX IDX_EVENT_VENUE ON events (venue)');
    }
}
